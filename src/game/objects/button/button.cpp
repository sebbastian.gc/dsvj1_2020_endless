#include "button.h"


using namespace endless;


button::button(Texture2D pad, float posY) {
	button::init(pad, posY);
}
button::button(Texture2D pad, float posX, float posY) {
	button::init(pad,posX, posY);	
}
button::~button() {
	UnloadTexture(_button);
	UnloadSound(fxButton);
}
void button::init(Texture2D pad, float posX, float posY) {
	_button = pad;
	fxButton = LoadSound("res/fx/button.mp3");
	numFrames = 3;
	frameHeight = _button.height / numFrames;
	sourceRec = { 0, 0, static_cast<float>(_button.width), static_cast<float>(frameHeight) };
	buttonBounds = { static_cast<float>(((screenWidth / 2) - (_button.width / 2 ))+ posX), static_cast<float>(0 - _button.height / numFrames / 2 + posY),static_cast<float>(_button.width), static_cast<float>(frameHeight) };
	buttonPos = { buttonBounds.x,buttonBounds.y };
	buttonState = 0;
	buttonAction = false;
	mousePoint = { 0.0f, 0.0f };
}
void button::init(Texture2D pad, float posY) {
	_button = pad;
	fxButton = LoadSound("res/fx/button.mp3");
	numFrames = 3;
	frameHeight = _button.height / numFrames;
	sourceRec = { 0, 0, static_cast<float>(_button.width), static_cast<float>(frameHeight) };
	buttonBounds = { static_cast<float>(screenWidth / 2 - _button.width / 2), static_cast<float>(screenHeight / 2 - _button.height / numFrames / 2 + posY),static_cast<float>(_button.width), static_cast<float>(frameHeight) };
	buttonPos = { buttonBounds.x,buttonBounds.y };
	buttonState = 0;
	buttonAction = false;
	mousePoint = { 0.0f, 0.0f };
}
void button::update() {
	mousePoint = GetMousePosition();
	buttonAction = false;
	if (CheckCollisionPointRec(mousePoint, buttonBounds)) {
		if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
			buttonState = 2;
		}
		else {
			buttonState = 1;
		}
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {
			buttonAction = true;
		}
	}
	else {
		buttonState = 0;
	}
	if (buttonAction) {
		PlaySound(fxButton);
	}
	sourceRec.y = static_cast<float>(buttonState * frameHeight);
}
void button::draw() {
	DrawTextureRec(_button, sourceRec, buttonPos, WHITE);
}
bool button::getButtonAction() {
	return buttonAction;
}