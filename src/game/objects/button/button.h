#ifndef BUTTON_H
#define BUTTON_H

#include "raylib.h"
#include "game/globales.h"

namespace endless {

	class button {
	private:
		Texture2D _button;
		Sound fxButton;
		int numFrames;
		int frameHeight;
		Rectangle sourceRec;
		Rectangle buttonBounds;
		Vector2 buttonPos;
		int buttonState;
		bool buttonAction;
		Vector2 mousePoint;

	public:
		button(Texture2D pad, float posY);
		button(Texture2D pad, float posX, float posY);
		~button();
		void init(Texture2D pad, float posX, float posY);
		void init(Texture2D pad, float posY);
		void update();
		void draw();
		bool getButtonAction();
	};
}
#endif

