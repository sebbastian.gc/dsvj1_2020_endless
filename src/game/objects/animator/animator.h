#ifndef ANIMATOR_H
#define ANIMATOR_H

#include "raylib.h"
namespace endless {
	class animator {
	private:
		Texture2D texture;
		Rectangle rec;
		Vector2 pos;
		int quantitySprites;
		float currentFrame;
		int framesCounter;
		float framesSpeed;
	public:
		animator(Texture2D anim, Vector2 _pos, int quantity);
		~animator();
		void init();
		void update(Vector2 _pos);
		void setCurrentFrame(float frame);
		void draw();
	};
}
#endif
