#include "animator.h"
#include "game/globales.h"
#include "game/input/input.h"

using namespace endless;

animator::animator(Texture2D anim,Vector2 _pos,int quantity) {
	texture = anim;	
	pos = _pos;
	quantitySprites = quantity;
	rec = { 0.0f, 0.0f, static_cast<float>(texture.width / quantitySprites), static_cast<float>(texture.height) };
	currentFrame = 0.0f;
	framesCounter = 0;
	framesSpeed = 10.0f;	
}
animator::~animator() {

}
void animator::init() {	
	currentFrame = 0.0f;	
}
void animator::update(Vector2 _pos) {	
	framesCounter++;
	if (framesCounter >= (frameRate / framesSpeed)){
		framesCounter = 0;		

		if (currentFrame == 0 && curretnAnim == ANIMATIONS::JUMP) {	jumpCollider = true;}
		if (currentFrame == 0 && curretnAnim == ANIMATIONS::ATACK) { atackCollider = true; }	

		currentFrame += frameRate * GetFrameTime();		
		
		if (currentFrame >= quantitySprites - 6 && curretnAnim == ANIMATIONS::JUMP) {
			isJumping = true;
		}
		
		if (currentFrame >= quantitySprites - 2 && curretnAnim == ANIMATIONS::DEATH) {
			input::initGame = false;	
			isDeath = true;			
		}
		if (currentFrame >= quantitySprites - 1 && curretnAnim != ANIMATIONS::RUN) {
			curretnAnim = ANIMATIONS::RUN; 
			currentFrame = 0.0f;
		}			
		if (currentFrame > quantitySprites - 1) { currentFrame = 0.0f; }

		rec.x = static_cast<float>(currentFrame * texture.width / quantitySprites);
	}
	pos = _pos;
}
void animator::setCurrentFrame(float frame) {
	currentFrame = frame;
}
void animator::draw() {
	
	DrawTextureRec(texture, rec, pos, WHITE);
}
	