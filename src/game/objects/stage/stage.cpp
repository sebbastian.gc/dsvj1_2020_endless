#include "stage.h"
#include "game/globales.h"


using namespace endless;

stage::stage(float posY) {
	stage::init( posY);
}
stage::~stage() {	
	UnloadTexture(obsOne);
	UnloadTexture(obsTwo);
	UnloadTexture(obsThree);
	UnloadTexture(obsFour);
	UnloadTexture(obsFive);
	UnloadTexture(coinTexture);

	for (short i = 0; i < size; i++){
		delete coins[i];
		coins[i] = NULL;
	}

	delete obsUnBreak;
	delete obsBreakable;
	delete obsUnbreakable;
	delete obsUnbreakableTwo;
	delete obsBreakableTwo;

	obsUnbreakableTwo = NULL;
	obsBreakableTwo = NULL;
	obsUnBreak = NULL;
	obsBreakable = NULL;
	obsUnbreakable = NULL;
}

void stage::reboot(short id) {
	if(id == obsUnbreakable->getID()){ obsUnbreakable->init(static_cast<float>(screenHeight - groundOne.height)); }
	if (id == obsBreakable->getID()) { obsBreakable->init(static_cast<float>(screenHeight - groundOne.height)); }
	if (id == obsUnBreak->getID()) { obsUnBreak->init(static_cast<float>(screenHeight - groundOne.height)); }
	if (id == obsUnbreakableTwo->getID()) { obsUnbreakableTwo->init(static_cast<float>(screenHeight - groundOne.height)); }
	if (id == obsBreakableTwo->getID()) { obsBreakableTwo->init(static_cast<float>(screenHeight - groundOne.height)); }
}
void stage::rebootCoin(short i) {
	coins[i]->init(static_cast<float>(screenHeight - groundOne.height));
}

Rectangle stage::getObsUnbreakable() {	
	return obsUnbreakable->getRec();
}
Rectangle stage::getObsBreak() {
	return obsBreakable->getRec();
}
Rectangle stage::getObsUnbreak() {
	return obsUnBreak->getRec();
}
Rectangle stage::getObsUnbreakableTwo() {
	return obsUnbreakableTwo->getRec();
}
Rectangle stage::getObsBreakTwo() {
	return obsBreakableTwo->getRec();
}
Rectangle stage::getGroundOne() {
	return groundOne;
}
Rectangle stage::getRecCoin(short i) {	
	return coins[i]->getRec();
}

bool stage::isActivated(short id) {
	if (id == obsUnbreakable->getID() && !obsUnbreakable->getActivated()) { return true; }
	else if (id == obsBreakable->getID() && !obsBreakable->getActivated()) { return true; }
	else if (id == obsUnBreak->getID() && !obsUnBreak->getActivated()) { return true; }
	else if (id == obsUnbreakableTwo->getID() && !obsUnbreakableTwo->getActivated()) { return true; }
	else if (id == obsBreakableTwo->getID() && !obsBreakableTwo->getActivated()) { return true; }
	else return false;
}
bool stage::isActivatedCoin(short id) {
	short aux = 0;
	for (short i = 0; i < size; i++){
		if (id == coins[i]->getID() && !coins[i]->getActivated()) { return true; }
		else aux++;		
		if (aux == size - 1) { return false; }
	}	
}
bool stage::timerObs(float _time) {
	timeObs++;
	if (timeObs / frameRate >= _time) {
		timeObs = 0;
		return true;
	}
	else return false;	
}
bool stage::timerCoin(float _time) {
	timeCoin++;
	if (timeCoin / frameRate >= _time) {
		timeCoin = 0;
		return true;
	}
	else return false;
}
void stage::spawn() {
	if (timerObs(timeSpawn)) {
		do {
			active = rand() % (5) + 1;			
		} while (!stage::isActivated(active));

		switch (active) {
			case 1:
				obsUnbreakable->setActivated(true);
				break;
			case 2:
				obsBreakable->setActivated(true);
				break;
			case 3:
				obsUnBreak->setActivated(true);
				break;
			case 4:
				obsUnbreakableTwo->setActivated(true);
				break;
			case 5:
				obsBreakableTwo->setActivated(true);
				break;
		}
	}
	if (timerCoin(timeSpawn)) {
		do {
			activeCoin = rand() % (size);
		} while (!stage::isActivatedCoin(activeCoin));

		coins[activeCoin]->setActivated(true);
	}
}

void stage::setActivatedObs(bool act, short id) {
	if (id == obsUnbreakable->getID()) { obsUnbreakable->setActivated(act); }
	if (id == obsBreakable->getID()) { obsBreakable->setActivated(act);	}
	if (id == obsUnBreak->getID()) { obsUnBreak->setActivated(act);	}
	if (id == obsUnbreakableTwo->getID()) { obsUnbreakableTwo->setActivated(act);	}
	if (id == obsBreakableTwo->getID()) { obsBreakableTwo->setActivated(act);}
}
void stage::setActiveCoins(bool act,short i) {
	coins[i]->setActivated(act);
}
void stage::updateCoin() {
	for (short i = 0; i < size; i++){
		coins[i]->update();
	}
}
void stage::drawCoin() {
	for (short i = 0; i < size; i++){
		coins[i]->draw();
	}
}

void stage::init(float posY) {	
	obsOne= LoadTexture("res/assets/obsOne.png");
	obsTwo = LoadTexture("res/assets/obsTwo.png");
	obsThree = LoadTexture("res/assets/obsThree.png");
	obsFour = LoadTexture("res/assets/obsOne.png");
	obsFive= LoadTexture("res/assets/obsTwo.png");
	coinTexture= LoadTexture("res/assets/coin.png");
	
	groundOne.height = 5.0f;
	groundOne.width = static_cast<float>(screenWidth );
	groundOne.x = 0.0f;
	groundOne.y = static_cast<float>(screenHeight - groundOne.height - posY);
	timeObs = 0.0f;
	timeCoin = 0.0f;
	
	switch (currentResolution){
		case endless::RESOLUTION::ONE:
			timeSpawn = 2.3f;
			break;
		case endless::RESOLUTION::TWO:
			timeSpawn = 3.0f;
			break;
		case endless::RESOLUTION::THREE:
			timeSpawn = 3.5f;
			break;	
	}
	active = 0;
	activeCoin = 0;

	obsUnbreakable = new obstacle(obsOne, static_cast<float>(screenHeight- groundOne.height - posY),1);
	obsBreakable = new obstacle(obsTwo, static_cast<float>(screenHeight - groundOne.height - posY), 2);
	obsUnBreak = new obstacle(obsThree, static_cast<float>(screenHeight - groundOne.height - posY), 3);
	obsUnbreakableTwo=new obstacle(obsFour, static_cast<float>(screenHeight - groundOne.height - posY), 4);
	obsBreakableTwo= new obstacle(obsFive, static_cast<float>(screenHeight - groundOne.height - posY), 5);
	for (short i = 0; i < size; i++){
		coins[i] = new coin(coinTexture, static_cast<float>(screenHeight - groundOne.height - posY), i);
	}
}
void stage::update() {	
	stage::spawn();	
	stage::updateCoin();
	obsUnbreakable->update();
	obsBreakable->update();	
	obsUnBreak->update();
	obsUnbreakableTwo->update();
	obsBreakableTwo->update();
}
void stage::draw() {
	stage::drawCoin();
	obsUnbreakable->draw();
	obsBreakable->draw();
	obsUnBreak->draw();
	obsUnbreakableTwo->draw();
	obsBreakableTwo->draw();
	
#if _DEBUG
	DrawRectangleLines(static_cast<int>(groundOne.x), static_cast<int>(groundOne.y), static_cast<int>(groundOne.width), static_cast<int>(groundOne.height), RED);
#endif 
}


