#ifndef STAGE_H
#define STAGE_H

#include "raylib.h"
#include "game/objects/obstacle/obstacle.h"
#include "game/objects/coin/coin.h"
#include "game/globales.h"

namespace endless {

	class stage{
	private:		
		Texture2D obsOne;
		Texture2D obsTwo;
		Texture2D obsThree;
		Texture2D obsFour;
		Texture2D obsFive;
		Texture2D coinTexture;

		Rectangle groundOne;
		obstacle* obsUnbreakable;
		obstacle* obsBreakable;
		obstacle* obsUnBreak;
		obstacle* obsUnbreakableTwo;
		obstacle* obsBreakableTwo;
		coin* coins[size];
		float timeObs;
		float timeCoin;
		float timeSpawn;
		short active;
		short activeCoin;


	public:
		stage(float posY);
		~stage();

		void reboot(short id);	
		void rebootCoin(short i);
		Rectangle getObsUnbreakable();
		Rectangle getObsBreak();
		Rectangle getObsUnbreak();	
		Rectangle getObsUnbreakableTwo();
		Rectangle getObsBreakTwo();
		Rectangle getGroundOne();
		Rectangle getRecCoin(short i);
		bool isActivated(short id);
		bool isActivatedCoin(short id);
		bool timerObs(float _time);
		bool timerCoin(float _time);
		void spawn();
		void setActivatedObs(bool act,short id);
		void setActiveCoins(bool act,short i);
		void updateCoin();
		void drawCoin();

		void init(float posY);
		void update();
		void draw();		
	};
}
#endif
