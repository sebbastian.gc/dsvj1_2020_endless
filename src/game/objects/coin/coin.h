#ifndef COIN_H
#define COIN_H

#include "raylib.h"

namespace endless {
	class coin {
	private:
		Texture2D _coin;
		Rectangle rec;
		int speed;
		short ID;
		bool activated;
		short distance;		
	public:
		coin (Texture2D _coin, float posy, short _ID);
		~coin();

		Rectangle getRec();
		short getID();
		bool getActivated();
		void setActivated(bool act);
		void reSize();

		void init(float posy);
		void update();
		void draw();
	};
}
#endif
