#include "coin.h"
#include "game/globales.h"

using namespace endless;

coin::coin(Texture2D obs, float posy, short _ID) {
	_coin = obs;
	coin::reSize();	
	distance = 6;
	rec.x = static_cast<float>(screenWidth + _coin.width*distance);
	rec.y = posy - _coin.height;
	rec.width = static_cast<float>(_coin.width);
	rec.height = static_cast<float>(_coin.height);
	ID = _ID;
	activated = false;
	speed = 200;
	
}
coin::~coin() {
	UnloadTexture(_coin);
	
}
Rectangle coin::getRec() {
	return rec;
}
short coin::getID() {
	return ID;
}
bool coin::getActivated() {
	return activated;
}
void coin::setActivated(bool act) {
	activated = act;
}
void coin::reSize() {
	float percentWid = 0.333f;
	float percentHed = 0.4f;
	if (screenWidth == 800 && screenHeight == 700) {
		_coin.width += static_cast<int>(_coin.width * percentWid);
		_coin.height += static_cast<int>(_coin.height * percentHed);
	}
	else if (screenWidth == 1000 && screenHeight == 900) {
		_coin.width += static_cast<int>(_coin.width * (percentWid * 2.0f));
		_coin.height += static_cast<int>(_coin.height * (percentHed * 2.0f));
	}
}

void coin::init(float posy) {
	rec.x = static_cast<float>(screenWidth + _coin.width*distance);
	rec.y = posy - _coin.height;
}
void coin::update() {
	if (activated) {
		rec.x -= speed * GetFrameTime();
		if (rec.x <= 0 - _coin.width) {
			rec.x = static_cast<float>(screenWidth + _coin.width*distance);
			activated = false;
		}
	}

}
void coin::draw() {
#if _DEBUG
	DrawRectangleLines(static_cast<int>(rec.x), static_cast<int>(rec.y), static_cast<int>(rec.width), static_cast<int>(rec.height), RED);
#endif
	DrawTexture(_coin, static_cast<int>(rec.x), static_cast<int>(rec.y), RAYWHITE);
}
