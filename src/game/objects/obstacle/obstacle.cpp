#include "obstacle.h"
#include "game/globales.h"

using namespace endless;

obstacle::obstacle(Texture2D obs, float posy, short _ID) {
	_obstacle = obs;
	obstacle::reSize();
	rec.x = static_cast<float>(screenWidth + _obstacle.width);
	rec.y = posy-_obstacle.height;
	rec.width = static_cast<float>(_obstacle.width);
	rec.height = static_cast<float>(_obstacle.height);
	ID = _ID;
	activated = false;
	speed = 200;
}
obstacle::~obstacle() {
	UnloadTexture(_obstacle);
}
Rectangle obstacle::getRec() {
	return rec;
}
short obstacle::getID() {
	return ID;
}
bool obstacle::getActivated() {
	return activated;
}
void obstacle::setActivated(bool act) {
	activated = act;
}
void obstacle::reSize() {
	float percentWid = 0.333f;
	float percentHed = 0.4f;
	if (screenWidth == 800 && screenHeight == 700) {		
		_obstacle.width += static_cast<int>(_obstacle.width * percentWid);
		_obstacle.height += static_cast<int>(_obstacle.height * percentHed);
	}
	else if (screenWidth == 1000 && screenHeight == 900) {		
		_obstacle.width += static_cast<int>(_obstacle.width * (percentWid * 2.0f));
		_obstacle.height += static_cast<int>(_obstacle.height * (percentHed * 2.0f));
	}
}

void obstacle::init(float posy) {
	rec.x = static_cast<float>(screenWidth + _obstacle.width);
	rec.y = posy - _obstacle.height;
}
void obstacle::update() {
	if(activated){
		rec.x -= speed * GetFrameTime();
		if (rec.x <= 0 - _obstacle.width) {
			rec.x = static_cast<float>(screenWidth + _obstacle.width);
			activated = false;
		}
	}
	
}
void obstacle::draw() {

#if _DEBUG
	DrawRectangleLines(static_cast<int>(rec.x), static_cast<int>(rec.y), static_cast<int>(rec.width), static_cast<int>(rec.height),RED);
#endif

	DrawTexture(_obstacle, static_cast<int>(rec.x), static_cast<int>(rec.y), RAYWHITE);
}
