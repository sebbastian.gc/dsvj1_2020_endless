#ifndef OBSTACLE_H
#define OBSTACLE_H

#include "raylib.h"

namespace endless {
	class obstacle {
	private:
		Texture2D _obstacle;
		Rectangle rec;
		int speed;
		short ID;
		bool activated;
	public:
		obstacle(Texture2D obs,float posy, short _ID);
		~obstacle();

		Rectangle getRec();
		short getID();
		bool getActivated();
		void setActivated(bool act);
		void reSize();	

		void init(float posy);
		void update();
		void draw();
	};
}
#endif
