#ifndef HERO_H
#define HERO_H

#include "raylib.h"
#include "game/objects/animator/animator.h"
#include "game/objects/stage/stage.h"

namespace endless {

	class hero {
	private:
		animator* runAnim;
		animator* atackAnim;
		animator* jumpAnim;
		animator* deathAnim;

		Texture2D runAmin;
		Texture2D jumpAmin;
		Texture2D atackAmin;
		Texture2D deathAmin;
		Texture2D life1;
		Texture2D life2;
		Texture2D life3;
		
		Vector2 pos;
		Rectangle boxCollider;
		Color color;

		short score;
		float jumpSpeed;
		short  life;		
		int quantitySprites;
		float auxHeight;
		float auxWidth;
	public:
		hero();
		~hero();

		void init();
		void setPos(float _y);
		void setColor(Color _color);
		void setSpeed(float _speed);
		void setScore(short _score);
		void setLife(short _life);
		void setFrame(float frame);

		Rectangle getBoxCollider();
		Vector2 getPos();
		Color getColor();
		float getSpeed();
		short getScore();
		short getLife();
		void reSize();
		void jump(stage* _stage);
		void atack();
		void updateCollider();
		void lifes();

		void update();
		void draw();
	};
}
#endif
