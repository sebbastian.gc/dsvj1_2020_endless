#include "hero.h"
#include "game/globales.h"
#include "game/input/input.h"
#include "game/scenes/gameplay/gameplay.h"

using namespace endless;

bool endless::isJumping;
bool endless::attacking;

hero::hero() {
	hero::init();
}
hero::~hero() {
	UnloadTexture(runAmin);
	UnloadTexture(atackAmin);
	UnloadTexture(jumpAmin);
	UnloadTexture(deathAmin);
	UnloadTexture(life1);
	UnloadTexture(life2);
	UnloadTexture(life3);

	delete deathAnim;
	delete jumpAnim;
	delete runAnim;
	delete atackAnim;

	deathAnim = NULL;
	jumpAnim = NULL;
	runAnim = NULL;
	atackAnim = NULL;
}
void hero::init() {
	runAmin= LoadTexture("res/assets/H_run.png");
	atackAmin = LoadTexture("res/assets/H_atack.png");
	jumpAmin = LoadTexture("res/assets/H_jump.png");
	deathAmin = LoadTexture("res/assets/H_death.png");	
	life1 = LoadTexture("res/assets/life_1.png");
	life2 = LoadTexture("res/assets/life_2.png");
	life3 = LoadTexture("res/assets/life_3.png");

	quantitySprites = 6;
	pos.x = static_cast<float>(screenWidth / 10.0f);
	pos.y = static_cast<float>(screenHeight / 1.18f);
	boxCollider.x = pos.x+ static_cast<float>(screenWidth / 20.0f);
	boxCollider.y = pos.y+ static_cast<float>(screenHeight / 50.0f);	
	boxCollider.width = 45.0f;
	boxCollider.height = 65.0f;
	jumpSpeed = 150.0f;	
	hero::reSize();
	runAnim = new animator(runAmin,pos, quantitySprites );
	atackAnim = new animator(atackAmin,pos, quantitySprites +1);
	jumpAnim = new animator(jumpAmin, pos, quantitySprites +6);
	deathAnim = new animator(deathAmin, pos, quantitySprites +2);	
	color = GREEN;
	isJumping = false;
	attacking = false;
	auxHeight = boxCollider.height;
	auxWidth = boxCollider.width;
	life = 3;
	curretnAnim = ANIMATIONS::RUN;
}
void hero::setPos( float _y) {	
	pos.y = _y;
}
void hero::setColor(Color _color) {
	color = _color;
}
void hero::setSpeed(float _speed) {
	jumpSpeed = _speed;
}
void hero::setScore(short _score) {
	score = _score;
}
void hero::setLife(short _life) {
	life = _life;
}
void hero::setFrame(float frame) {
	atackAnim->setCurrentFrame(frame);
	deathAnim->setCurrentFrame(frame);
	runAnim->setCurrentFrame(frame);
	runAnim->setCurrentFrame(frame);
}

Rectangle hero::getBoxCollider() {
	return boxCollider;
}
Vector2 hero::getPos() {
	return pos;
}
Color hero::getColor() {
	return color;
}
float hero::getSpeed() {
	return jumpSpeed;
}
short hero::getScore() {
	return score;
}
short hero::getLife() {
	return life;
}
void hero::jump(stage* _stage) {
	
	if (CheckCollisionRecs(boxCollider, _stage->getGroundOne())) {
		isGrounded = true;	
		isJumping = false;
	}
	else{
		isGrounded = false;
		if(!isGrounded && curretnAnim != ANIMATIONS::JUMP){ pos.y += jumpSpeed * GetFrameTime(); }
	}
	if (curretnAnim == ANIMATIONS::JUMP ) {
		isJumping  ? pos.y += jumpSpeed * GetFrameTime() : pos.y -= jumpSpeed * GetFrameTime();
	}
		 
}
void hero::atack(){
	if (curretnAnim == ANIMATIONS::ATACK) {
		attacking = true;
	}
	else attacking = false;
}
void hero::reSize() {
	float percentWid = 0.333f;
	float percentHed = 0.4f;
	if(screenWidth == 800 && screenHeight ==700){
		jumpSpeed += jumpSpeed * percentHed;		
		boxCollider.width += boxCollider.width* percentWid;
		boxCollider.height += boxCollider.height* percentHed;

		runAmin.width += static_cast<int>(runAmin.width * percentWid);
		runAmin.height += static_cast<int>(runAmin.height * percentHed);
		atackAmin.width += static_cast<int>(atackAmin.width * percentWid);
		atackAmin.height += static_cast<int>(atackAmin.height * percentHed);
		jumpAmin.width += static_cast<int>(jumpAmin.width * percentWid);
		jumpAmin.height += static_cast<int>(jumpAmin.height * percentHed);
		deathAmin.width += static_cast<int>(deathAmin.width * percentWid);
		deathAmin.height += static_cast<int>(deathAmin.height * percentHed);
		life1.width += static_cast<int>(life1.width*percentWid);
		life1.height += static_cast<int>(life1.height * percentHed);
		life2.width += static_cast<int>(life2.width * percentWid);
		life2.height += static_cast<int>(life2.height * percentHed);
		life3.width += static_cast<int>(life3.width * percentWid);
		life3.height += static_cast<int>(life3.height * percentHed);
	}
	else if  (screenWidth == 1000 && screenHeight == 900) {
		jumpSpeed += jumpSpeed * (percentHed * 2.0f);		
		boxCollider.width += boxCollider.width * (percentWid * 2.0f);
		boxCollider.height += boxCollider.height * (percentHed * 2.0f);

		runAmin.width += static_cast<int>(runAmin.width * (percentWid*2.0f));
		runAmin.height += static_cast<int>(runAmin.height * (percentHed * 2.0f));
		atackAmin.width += static_cast<int>(atackAmin.width * (percentWid * 2.0f));
		atackAmin.height += static_cast<int>(atackAmin.height * (percentHed * 2.0f));
		jumpAmin.width += static_cast<int>(jumpAmin.width * (percentWid * 2.0f));
		jumpAmin.height += static_cast<int>(jumpAmin.height * (percentHed * 2.0f));
		deathAmin.width += static_cast<int>(deathAmin.width * (percentWid * 2.0f));
		deathAmin.height += static_cast<int>(deathAmin.height * (percentHed * 2.0f));
		life1.width += static_cast<int>(life1.width * (percentWid * 2.0f));
		life1.height += static_cast<int>(life1.height * (percentHed * 2.0f));
		life2.width += static_cast<int>(life2.width * (percentWid * 2.0f));
		life2.height += static_cast<int>(life2.height * (percentHed * 2.0f));
		life3.width += static_cast<int>(life3.width * (percentWid * 2.0f));
		life3.height += static_cast<int>(life3.height * (percentHed * 2.0f));
	}
}
void hero::updateCollider() {	
	boxCollider.x = pos.x + static_cast<float>(screenWidth / 20.0f);
	boxCollider.y = pos.y + static_cast<float>(screenHeight / 50.0f);

	if (jumpCollider && curretnAnim == ANIMATIONS::JUMP) {
		auxHeight = boxCollider.height;
		boxCollider.height = boxCollider.height *0.769f;		
		jumpCollider = false;
	}
	if (atackCollider && curretnAnim == ANIMATIONS::ATACK) {
		auxWidth = boxCollider.width;
		boxCollider.width += boxCollider.width *0.335f;
		atackCollider = false;
	}
	if (curretnAnim != ANIMATIONS::JUMP){
		boxCollider.height = auxHeight;
	}
	if (curretnAnim != ANIMATIONS::ATACK) {
		boxCollider.width = auxWidth;
	}
	
}
void hero::lifes() {
	if(life ==1){ DrawTexture(life1, 0, 0, WHITE); }
	else if (life == 2) { DrawTexture(life2, 0, 0, WHITE); }
	else if (life == 3) { DrawTexture(life3, 0, 0, WHITE); }
}

void hero::update() {
	hero::updateCollider();
	if (isDeath) {
		jumpAnim->init();		
		isDeath = false;
	}
	switch (curretnAnim){
		case ANIMATIONS::RUN:				
				runAnim->update(pos);
				break;
		case ANIMATIONS::ATACK:
				atackAnim->update(pos);
				break;
		case ANIMATIONS::JUMP:				
				jumpAnim->update(pos);
				
				break;
		case ANIMATIONS::DEATH:
				deathAnim->update(pos);
				break;	
	}	
	
}
void hero::draw() {
#if _DEBUG
	DrawRectangleLines(static_cast<int>(boxCollider.x), static_cast<int>(boxCollider.y), static_cast<int>(boxCollider.width), static_cast<int>(boxCollider.height), GREEN);
#endif 

	lifes();
	switch (curretnAnim) {
		case ANIMATIONS::RUN:
				runAnim->draw();
				break;
		case ANIMATIONS::ATACK:
				atackAnim->draw();
				break;
		case ANIMATIONS::JUMP:			
				jumpAnim->draw();
				break;
		case ANIMATIONS::DEATH:
				deathAnim->draw();
				break;
	}
}