#include "gameLoop.h"

#include "game/globales.h"
#include "game/input/input.h"
#include "game/scenes/menu/menu.h"
#include "game/scenes/gamePlay/gamePlay.h"

using namespace endless;

int endless::screenWidth;
int endless::screenHeight;
bool endless::initscenes;
SCENES endless::currentScene;
RESOLUTION endless::currentResolution;

namespace endless {
    namespace gameLoop {

        void static init() {
            srand(static_cast<unsigned int>(time(NULL)));
            screenWidth = 600;
            screenHeight = 500;
            currentResolution = RESOLUTION::ONE;
            currentScene = SCENES::MENU;
            initscenes = true;
            InitWindow(screenWidth, screenHeight, " ");
            SetTargetFPS(static_cast<int>(frameRate));
            InitAudioDevice();
            input::init();             
        }
        void static input() {
            input::update();
        }
        void static update() {
            switch (currentScene) {
            case SCENES::MENU:
                if (initscenes) { menu::init(); }
                menu::update();
                break;
            case SCENES::GAMEPLAY: 
                if (initscenes) { gameplay::init(); }
                gameplay::update();               
                break;
            }
        }
        void static draw() {
            BeginDrawing();
            ClearBackground(BLACK);
            switch (currentScene) {
            case SCENES::MENU:
                menu::draw();
                break;
            case SCENES::GAMEPLAY:
                gameplay::draw();
                break;
            }
            EndDrawing();
        }
        void static deinit() {
            menu::deinit();
            gameplay::deinit(); 
            
        }
        void run() {
            init();
            while (!WindowShouldClose()) {
                input();               
                update();
                draw();
            }
            CloseAudioDevice();
            deinit();
            
        }

    }
}