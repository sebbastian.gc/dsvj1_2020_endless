#ifndef GAMELOOP_H
#define GAMELOOP_H

#include "raylib.h"

namespace endless {

	namespace gameLoop {
		void run();
	}

}
#endif
