#include"background.h"

namespace endless {

	namespace background {

		Texture2D background;
		Texture2D midground;
		Texture2D foreground;

		Vector2 back;
		Vector2 posBack;
		Vector2 mid;
		Vector2 posMid;
		Vector2 fore;
		Vector2 posFore;
		Color color;

		float scrollingBack;
		float scrollingMid;
		float scrollingFore;

		void static reSize() {
			background.height = screenHeight;
			midground.height = screenHeight;
			foreground.height = screenHeight;
		}

		void init() {
			background = LoadTexture("res/assets/background.png");
			midground = LoadTexture("res/assets/midground.png");
			foreground = LoadTexture("res/assets/foreground.png");
			
			reSize();
			color = RAYWHITE;
			scrollingBack = 0.0f;
			scrollingMid = 0.0f;
			scrollingFore = 0.0f;
			back = { scrollingBack, 0 };
			posBack = { background.width + scrollingBack, 0 };
			mid = { scrollingMid, 0 };
			posMid = { midground.width + scrollingMid, 0 };
			fore = { scrollingFore, 0 };
			posFore = { foreground.width + scrollingFore, 0 };
		}
		void update() {
			scrollingBack -= 30.0f * GetFrameTime();
			scrollingMid -= 60.0f * GetFrameTime();
			scrollingFore -= 120.0f * GetFrameTime();

			if (scrollingBack <= -background.width) scrollingBack = 0;
			if (scrollingMid <= -midground.width) scrollingMid = 0;
			if (scrollingFore <= -foreground.width) scrollingFore = 0;

			back = { scrollingBack, 0 };
			posBack = { background.width + scrollingBack, 0 };
			mid = { scrollingMid, 0 };
			posMid = { midground.width + scrollingMid, 0 };
			fore = { scrollingFore, 0 };
			posFore = { foreground.width + scrollingFore, 0 };
		}
		void draw() {
			DrawTextureEx(background, back, 0.0f, 1.0f, color);
			DrawTextureEx(background, posBack, 0.0f, 1.0f, color);

			DrawTextureEx(midground, mid, 0.0f, 1.0f, color);
			DrawTextureEx(midground, posMid, 0.0f, 1.0f, color);

			DrawTextureEx(foreground, fore, 0.0f, 1.0f, color);
			DrawTextureEx(foreground, posFore, 0.0f, 1.0f, color);
		}
		void deinit() {
			UnloadTexture(background);
			UnloadTexture(midground);
			UnloadTexture(foreground);
		}
	}
}