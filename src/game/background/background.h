#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "raylib.h"
#include "game/globales.h"

namespace endless {

	namespace background {
		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
