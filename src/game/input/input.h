#ifndef INPUT_H
#define INPUT_H

#include "raylib.h"

namespace endless{

	namespace input {

		extern bool pause;
		extern bool initGame;
		extern bool up;
		extern bool down;
		extern bool mute;

		void init();
		void update();
	}
}
#endif
