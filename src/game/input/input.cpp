#include "input.h"
#include "game/globales.h"


using namespace endless;

ANIMATIONS endless::curretnAnim;
bool endless::isGrounded;
bool input::pause;
bool input::initGame;
bool input::up;
bool input::down;
bool input::mute;

namespace endless {

	namespace input {		

		void init() {
			curretnAnim = ANIMATIONS::RUN;
			isGrounded = false;
			pause = false;
			initGame = false;
			up = false;
			down = false;
		}
		void update() {			
			if (IsKeyPressed(KEY_A) && isGrounded && initGame && !gameOver) { curretnAnim = ANIMATIONS::ATACK; }
			if (IsKeyPressed(KEY_S)&& isGrounded && !attacking && initGame && !gameOver) { curretnAnim = ANIMATIONS::JUMP; }
			if (IsKeyPressed(KEY_SPACE) && initGame && !gameOver) { !pause ? pause = true : pause = false; }
			if (IsKeyPressed(KEY_ENTER)) { initGame = true; }
			if (IsKeyPressed(KEY_UP) && isGrounded && !attacking && initGame) { up = true; };
			if (IsKeyPressed(KEY_DOWN) && isGrounded && !attacking && initGame) { down = true; };
			if (IsKeyPressed(KEY_Q) && !pause) { !mute ? mute = true : mute = false; }

			

		}
	}
}