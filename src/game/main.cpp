#include "gameLoop/gameLoop.h"

using namespace endless;

int main() {
    gameLoop::run();

    return 0;
}