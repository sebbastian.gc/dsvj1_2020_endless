#ifndef GLOBAL_H
#define GLOBAL_H

#include <iostream>
#include <stdlib.h>
#include <time.h>

namespace endless {

	extern int screenWidth;
	extern int screenHeight;
	extern bool initscenes;
	extern bool isGrounded;
	extern bool isJumping;
	extern bool	attacking;
	extern bool isDeath;
	extern bool jumpCollider;
	extern bool atackCollider;
	const  float frameRate = 60.0f;
	const short size = 6;
	extern bool  gameOver;
	extern bool  block;

	enum class SCENES { MENU, GAMEPLAY };	
	enum class SCREENS { MENU, CREDITS, INSTRUCTIONS, GAMEPLAY,OPTIONS };
	enum class ANIMATIONS { RUN, ATACK, JUMP, DEATH };
	enum class RESOLUTION {ONE ,TWO, THREE};
	enum class STAGE{DOWN,MID,UP};
	extern ANIMATIONS curretnAnim;
	extern SCENES currentScene;
	extern SCREENS currentScreen;
	extern RESOLUTION currentResolution;
	extern STAGE currentStage;
}
#endif
