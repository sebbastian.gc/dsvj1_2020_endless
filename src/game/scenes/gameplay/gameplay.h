#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"
#include "game/globales.h"

namespace endless {
	namespace gameplay {		
		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
