#include "gameplay.h"
#include "game/objects/hero/hero.h"
#include "game/objects/stage/stage.h"
#include "game/objects/button/button.h"
#include "game/input/input.h"
#include "game/background/background.h"

using namespace endless;

bool endless::jumpCollider;
bool endless::atackCollider;
bool endless::isDeath;
bool endless::gameOver;
bool endless::block;
STAGE endless::currentStage;

namespace endless {
	namespace gameplay {

		Texture2D startTexture;
		Texture2D pauseTexture;
		Texture2D backTexture;
		Texture2D gameOverTexture;
		Texture2D scoreTexture;
		Texture2D winTexture;
		Music     gameplayMusic;

		button*   back;
		hero*     player;
		stage*	  stageDown;
		stage*	  stageMid;
		stage*	  stageUp;
		Sound     coinFx;
		
		short	  scoreWin;
		bool      changeStage;
		float     volume;
		int		  sizeFont;
		int       time;
		
		void static reSize() {
			pauseTexture.width = screenWidth;
			pauseTexture.height = screenHeight;
			startTexture.width = screenWidth;
			startTexture.height = screenHeight;
			gameOverTexture.width= screenWidth;
			gameOverTexture.height= screenHeight;
			scoreTexture.width= screenWidth;
			scoreTexture.height= screenHeight;
			winTexture.width = screenWidth;
			winTexture.height = screenHeight;
		}
		void static reSizeStage() {
			switch (currentResolution){
				case endless::RESOLUTION::ONE:
					stageDown = new stage(0);
					stageMid = new stage(165);
					stageUp = new stage(335);
					sizeFont = 30;
					break;
				case endless::RESOLUTION::TWO:
					stageDown = new stage(0);
					stageMid = new stage(232);
					stageUp = new stage(465);
					sizeFont = 40;
					break;
				case endless::RESOLUTION::THREE:
					stageDown = new stage(0);
					stageMid = new stage(300);
					stageUp = new stage(600);
					sizeFont = 50;
					break;			
			}
		}
		void static reboot() {			
			StopMusicStream(gameplayMusic);			
			currentScene = SCENES::MENU;
			input::initGame = false;
			input::pause = false;
			initscenes = true;
			gameOver = false;
			block = false;
			deinit();
		}	
		void static rebootStage(short id){
			input::pause = false;			
			input::initGame = true;			
			isDeath = false;

			switch (currentStage)			{
				case endless::STAGE::DOWN:
					stageDown->reboot(id);
					stageDown->setActivatedObs(false, id);
					break;
				case endless::STAGE::MID:
					stageMid->reboot(id);
					stageMid->setActivatedObs(false, id);
					break;
				case endless::STAGE::UP:
					stageUp->reboot(id);
					stageUp->setActivatedObs(false, id);
					break;			
			}			
		}
		void static pause() {
			back->update();
			if (back->getButtonAction()) {				
				reboot();
			}
		}
		void static collisionHeroObstacleUnBrake(Rectangle rec, short id) { 			
			if (CheckCollisionRecs(player->getBoxCollider(), rec )) {				
					curretnAnim = ANIMATIONS::DEATH;
					

					if(isDeath){
						player->setLife(player->getLife() - 1);						
					}	
					if (!input::initGame) { 
						rebootStage(id);
						block =false ;
					}
					
			}
		}
		void static collisionHeroObstacleBrake(Rectangle rec, short id) {
			if (CheckCollisionRecs(player->getBoxCollider(), rec)) {
				if (curretnAnim == ANIMATIONS::ATACK) {
					switch (currentStage) {
						case endless::STAGE::DOWN:
							stageDown->reboot(id);
							stageDown->setActivatedObs(false, id);
							break;
						case endless::STAGE::MID:
							stageMid->reboot(id);
							stageMid->setActivatedObs(false, id);
							break;
						case endless::STAGE::UP:
							stageUp->reboot(id);
							stageUp->setActivatedObs(false, id);
							break;
					}

				}
				else {
					curretnAnim = ANIMATIONS::DEATH;
					if (isDeath) { player->setLife(player->getLife() - 1); }
					if (!input::initGame) { rebootStage(id); }
				}
			}
		}
		void static collisionObstacle(stage* _satage) {
			collisionHeroObstacleUnBrake(_satage->getObsUnbreakable(), 1);
			collisionHeroObstacleBrake(_satage->getObsBreak(), 2);
			collisionHeroObstacleUnBrake(_satage->getObsUnbreak(), 3);
			collisionHeroObstacleUnBrake(_satage->getObsUnbreakableTwo(), 4);
			collisionHeroObstacleBrake(_satage->getObsBreakTwo(), 5);
		}
		void static collisionHeroCoins(stage* _satage) {
			for (short i = 0; i < size; i++){
				if (CheckCollisionRecs(player->getBoxCollider(), _satage->getRecCoin(i))) {
					if (curretnAnim != ANIMATIONS::DEATH) {
						PlaySound(coinFx);
						player->setScore(player->getScore()+10);
						_satage->rebootCoin(i);
						_satage->setActiveCoins(false,i);
					}
				}
			}
		}
		void static updateCollisionStages() {
			switch (currentStage){
				case endless::STAGE::DOWN:
					collisionObstacle(stageDown);
					break;
				case endless::STAGE::MID:
					collisionObstacle(stageMid);
					break;
				case endless::STAGE::UP:
					collisionObstacle(stageUp);
					break;			
			}
		}
		void static updateCollisionHeroCoins(){
			switch (currentStage) {
				case endless::STAGE::DOWN:
					collisionHeroCoins(stageDown);
					break;
				case endless::STAGE::MID:
					collisionHeroCoins(stageMid);
					break;
				case endless::STAGE::UP:
					collisionHeroCoins(stageUp);
					break;
			}
		}

		void static gameover() {
			if (player != NULL) {
				if (player->getLife() <= 0) {
					gameOver = true;
					back->update();
					if (back->getButtonAction()) {
						reboot();
					}
				}
			}			
		}
		void static win() {
			if (player != NULL) {
				if (player->getScore() >= scoreWin) {
					input::initGame = false;
					back->update();
					if (back->getButtonAction()) {
						reboot();
					}
				}
			}
		}
		void static changeHeroStage() {
			if (input::down && currentStage == STAGE::UP) { currentStage = STAGE::MID; input::down = false; changeStage = true; }
			if (input::down && currentStage == STAGE::MID) { currentStage = STAGE::DOWN; input::down = false; changeStage = true;}
			if (input::down && currentStage == STAGE::DOWN) { input::down = false; }

			if (input::up && currentStage == STAGE::DOWN) { currentStage = STAGE::MID; input::up = false; changeStage = true;}
			if (input::up && currentStage == STAGE::MID) { currentStage = STAGE::UP; input::up = false; changeStage = true;}
			if (input::up && currentStage == STAGE::UP) { input::up = false; }

			if (changeStage ) {
				switch (currentStage) {
				case endless::STAGE::DOWN:
					player->setPos(static_cast<float>(screenHeight / 1.18f));						
					break;
				case endless::STAGE::MID:
					switch (currentResolution) {
						case endless::RESOLUTION::ONE:
							player->setPos(255);
							break;
						case endless::RESOLUTION::TWO:
							player->setPos(336);							
							break;
						case endless::RESOLUTION::THREE:
							player->setPos(456);
							break;
					}					
					break;
				case endless::STAGE::UP:
					switch (currentResolution) {
						case endless::RESOLUTION::ONE:
							player->setPos(85);
							break;
						case endless::RESOLUTION::TWO:
							player->setPos(128);
							break;
						case endless::RESOLUTION::THREE:
							player->setPos(158);
							break;
					}					
					break;
				}
				changeStage = false;
			}
			
		}
		void static music() {
			UpdateMusicStream(gameplayMusic);
			if (input::mute) { volume = 0; }
			else volume = 0.2f;
			SetMusicVolume(gameplayMusic, volume);
			if (!input::pause) { PlayMusicStream(gameplayMusic); }
			else PauseMusicStream(gameplayMusic);
		}

		void static drawInit() {
			if (!gameOver) {
				if (!input::initGame && player->getScore() < scoreWin) {
					DrawTexture(startTexture, 0, 0, WHITE);
				}
			}
		}
		void static drawPause() {
			if (input::pause) {
				back->draw();
				DrawTexture(pauseTexture, 0, 0, WHITE);
			}
		}
		void static drawScore() {
			DrawTexture(scoreTexture, 0, 0, WHITE);
			DrawText(TextFormat("%01i", player->getScore()), static_cast<int>(screenWidth / 1.97f), static_cast<int>(screenHeight*0.006f), sizeFont , ORANGE);
		}
		void static drawWin() {
			if (player->getScore() >= scoreWin && !input::initGame) {
				back->draw();
				DrawTexture(winTexture, 0, 0, WHITE);
			}

		}
		void static drawGameOver() {
			if (gameOver) {
				back->draw();
				DrawTexture(gameOverTexture, 0, 0, WHITE);
			}
		}

		void init() {			
			backTexture = LoadTexture("res/assets/button_back.png");
			startTexture = LoadTexture("res/assets/start.png");
			pauseTexture = LoadTexture("res/assets/pause.png");
			gameOverTexture= LoadTexture("res/assets/gameOver.png");
			scoreTexture= LoadTexture("res/assets/score.png");
			winTexture= LoadTexture("res/assets/win.png");
			gameplayMusic = LoadMusicStream("res/fx/gameplay.mp3");
			coinFx = LoadSound("res/fx/coin.mp3");
			reSize();
			sizeFont = 30;
			background::init();
			back = new button(backTexture, static_cast<float>(screenWidth / 2.4f), 15.0f);
			player = new hero();
			reSizeStage();
			currentStage = STAGE::DOWN;
			volume = 0.2f;

			gameOver = false;
			initscenes = false;	
			jumpCollider = false;
			atackCollider=false;
			isDeath = false;
			changeStage = false;
			scoreWin = 100;
			block = false;
			time = 0;
		}
		void update() {	
			if (player != NULL ) {	
				music();
				if (!gameOver) {
					if (!input::pause) {
						if (input::initGame) {
							if (curretnAnim == ANIMATIONS::RUN){ changeHeroStage(); }
							if (curretnAnim != ANIMATIONS::ATACK && curretnAnim != ANIMATIONS::DEATH ) {
								stageDown->update();
								stageMid->update();
								stageUp->update();								
								background::update();
							}
							player->update();							
							if (currentStage == STAGE::UP) {player->jump(stageUp); }
							if (currentStage == STAGE::MID) { player->jump(stageMid); }
							if (currentStage == STAGE::DOWN) {player->jump(stageDown); }
							player->atack();							
							updateCollisionStages();							
							updateCollisionHeroCoins();
						}
					}
					else  pause();					
				}				
				win();
				gameover();
			}		
		}
		void draw() {	
			if (player != NULL) {
					background::draw();
					stageDown->draw();
					stageMid->draw();
					stageUp->draw();
					player->draw();
					drawInit();
					drawPause();
					drawScore();
					drawWin();
					drawGameOver();				
			}		
		}
		void deinit() {
			UnloadTexture(startTexture);
			UnloadTexture(pauseTexture);
			UnloadTexture(gameOverTexture);
			UnloadTexture(scoreTexture);
			UnloadTexture(winTexture);
			UnloadSound(coinFx);
			UnloadMusicStream(gameplayMusic);
			

			delete back;
			delete player;
			delete stageDown;
			delete stageMid;
			delete stageUp;

			stageDown = NULL;
			stageMid = NULL;
			stageUp = NULL;
			back = NULL;
			player = NULL;
		}
	}
}