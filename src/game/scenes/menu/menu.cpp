#include "menu.h"

#include "game/globales.h"
#include "game/input/input.h"

using namespace endless;

SCREENS endless::currentScreen;

namespace endless {
	namespace menu {

		Texture2D screenMenuTexture;
		Texture2D screenCreditsTexture;
		Texture2D screenOptionsTexture;
		Texture2D screenInstTexture;

		Texture2D playTexture;
		Texture2D optionsTexture;
		Texture2D creditsTexture;
		Texture2D exitTexture;
		Texture2D backTexture;
		Texture2D startTexture;
		Texture2D resOneTexture;
		Texture2D resTwoTexture;
		Texture2D resThreeTexture;
		Music     menuMusic;


		button* play;
		button* options;
		button* credits;
		button* exit;
		button* back;
		button* start;
		button* resolutionOne;
		button* resolutionTwo;
		button* resolutionThree;
		float   volume;

		void static initReSize() {
			play->init(playTexture, -75.0f);
			options->init(optionsTexture, 0.0f);
			credits->init(creditsTexture, 75.0f);
			exit->init(exitTexture, 150.0f);
			back->init(backTexture, static_cast<float>(screenWidth/2.4f), 15.0f);
			start->init(startTexture, static_cast<float>(screenHeight/2.5f));
			resolutionOne->init(resOneTexture, static_cast<float>(screenWidth /-4.0f), static_cast<float>(screenHeight / 2.2222f));
			resolutionTwo->init(resTwoTexture, static_cast<float>(screenWidth / -4.0f), static_cast<float>(screenHeight / 1.6666f));
			resolutionThree->init(resThreeTexture, static_cast<float>(screenWidth / -4.0f), static_cast<float>(screenHeight / 1.3333f));
		}
		void static reSize(int width, int height) {
			SetWindowSize(width, height);
			if (IsWindowResized()) {
				screenMenuTexture.width = width;
				screenMenuTexture.height = height;
				screenCreditsTexture.width = width;
				screenCreditsTexture.height = height;
				screenInstTexture.width = width;
				screenInstTexture.height = height;
				screenOptionsTexture.width = width;
				screenOptionsTexture.height = height;
				initReSize();
			}
		}
		void static reSizeScreens() {
			screenMenuTexture.width = screenWidth;
			screenMenuTexture.height = screenHeight;
			screenCreditsTexture.width = screenWidth;
			screenCreditsTexture.height = screenHeight;
			screenInstTexture.width = screenWidth;
			screenInstTexture.height = screenHeight;
			screenOptionsTexture.width = screenWidth;
			screenOptionsTexture.height = screenHeight;
		}
		void static playButton() {
			play->update();
			if (play->getButtonAction()) {				
				currentScreen = SCREENS::INSTRUCTIONS;
			}
		}
		void static startButton() {
			start->update();
			if (start->getButtonAction()) {
				deinit();
				currentScene = SCENES::GAMEPLAY;				
				initscenes=true;				
			}
		}

		void static optionsButton() {
			options->update();
			if (options->getButtonAction()) {
				currentScreen = SCREENS::OPTIONS;
			}
		}
		void static creditsButton() {
			credits->update();
			if (credits->getButtonAction()) {
				currentScreen = SCREENS::CREDITS;
			}
		}
		void static exitButton() {
			exit->update();
			if (exit->getButtonAction()) {
				CloseWindow();
			}
		}
		void static backButton() {
			back->update();
			if (back->getButtonAction()) {
				currentScreen = SCREENS::MENU;
			}
		}
		void static drawMenu() {
			DrawTexture(screenMenuTexture, 0, 0, WHITE);
			play->draw();
			options->draw();
			credits->draw();
			exit->draw();
		}
		void static resOneButton() {
			resolutionOne->update();
			if (resolutionOne->getButtonAction()) {				
				currentResolution = RESOLUTION::ONE;			
			}
		}
		void static resTwoButton() {
			resolutionTwo->update();
			if (resolutionTwo->getButtonAction()) {				
				currentResolution = RESOLUTION::TWO;
			}
		}
		void static resThreeButton() {
			resolutionThree->update();
			if (resolutionThree->getButtonAction()) {			
				currentResolution = RESOLUTION::THREE;
			}
		}	
		void static updateResolution() {			
			switch (currentResolution){
				case endless::RESOLUTION::ONE:
						screenWidth = 600;
						screenHeight = 500;
						reSize(screenWidth, screenHeight);
						break;
				case endless::RESOLUTION::TWO:
						screenWidth = 800;
						screenHeight = 700;
						reSize(screenWidth, screenHeight);
						break;
				case endless::RESOLUTION::THREE:
						screenWidth = 1000;
						screenHeight = 900;
						reSize(screenWidth, screenHeight);
						break;			
			}		
		}
		void static music() {
			UpdateMusicStream(menuMusic);
			if (input::mute) { volume = 0; }
			else volume = 0.2f;
			SetMusicVolume(menuMusic, volume);
			if (!input::pause) { PlayMusicStream(menuMusic); }
			else PauseMusicStream(menuMusic);
		}
		void init() {
			currentScreen = SCREENS::MENU;
			currentScene = SCENES::MENU;
			screenMenuTexture = LoadTexture("res/assets/screen_menu.png");
			screenCreditsTexture = LoadTexture("res/assets/screen_credits.png");
			screenInstTexture = LoadTexture("res/assets/screen_instructions.png");
			screenOptionsTexture = LoadTexture("res/assets/screen_options.png");

			playTexture = LoadTexture("res/assets/button_play.png");
			optionsTexture = LoadTexture("res/assets/button_options.png");
			creditsTexture = LoadTexture("res/assets/button_credits.png");
			exitTexture = LoadTexture("res/assets/button_exit.png");
			backTexture = LoadTexture("res/assets/button_back.png");
			startTexture = LoadTexture("res/assets/button_start.png");
			resOneTexture = LoadTexture("res/assets/button_600X500.png");
			resTwoTexture = LoadTexture("res/assets/button_800X700.png");
			resThreeTexture = LoadTexture("res/assets/button_1000x900.png");
			menuMusic = LoadMusicStream("res/fx/menu.mp3");
			reSizeScreens();

			play = new button(playTexture, -75.0f);
			options = new button(optionsTexture, 0.0f);
			credits = new button(creditsTexture, 75.0f);
			exit = new button(exitTexture, 150.0f);
			back = new button(backTexture, static_cast<float>(screenWidth / 2.4f), 15.0f);
			start = new button(startTexture, static_cast<float>(screenHeight / 2.5f));
			resolutionOne = new button(resOneTexture, static_cast<float>(screenWidth / -4.0f), static_cast<float>(screenHeight / 2.2222f));
			resolutionTwo = new button(resTwoTexture, static_cast<float>(screenWidth / -4.0f), static_cast<float>(screenHeight / 1.6666f));
			resolutionThree = new button(resThreeTexture, static_cast<float>(screenWidth / -4.0f), static_cast<float>(screenHeight / 1.3333f));	
			initscenes = false;
			volume = 0.2f;
		}
		void update() {	
			if (play != NULL) {
				music();
				updateResolution();
				switch (currentScreen) {
					case SCREENS::MENU:
						playButton();
						optionsButton();
						creditsButton();
						exitButton();
						break;
					case SCREENS::CREDITS:
						backButton();
						break;
					case SCREENS::OPTIONS:
						resOneButton();
						resTwoButton();
						resThreeButton();
						backButton();
						break;
					case SCREENS::INSTRUCTIONS:
						startButton();
						break;
				}
			}
		}
		void draw() {
			if (play != NULL) {
				switch (currentScreen) {
					case SCREENS::MENU:
						drawMenu();
						break;
					case SCREENS::CREDITS:
						DrawTexture(screenCreditsTexture, 0, 0, WHITE);
						back->draw();
						break;
					case SCREENS::INSTRUCTIONS:
						DrawTexture(screenInstTexture, 0, 0, WHITE);
						start->draw();
						break;
					case SCREENS::OPTIONS:
						DrawTexture(screenOptionsTexture, 0, 0, WHITE);
						resolutionOne->draw();
						resolutionTwo->draw();
						resolutionThree->draw();
						back->draw();
						break;
				}
			}
		}
		void deinit() {
			UnloadTexture(screenMenuTexture);
			UnloadTexture(screenInstTexture);
			UnloadTexture(screenCreditsTexture);
			UnloadTexture(screenOptionsTexture);
			UnloadMusicStream(menuMusic);
			delete play;
			delete options;
			delete credits;
			delete exit;
			delete back;
			delete start;
			delete resolutionOne;
			delete resolutionTwo;
			delete resolutionThree;

			play=NULL;
			options=NULL;
			credits=NULL;
			exit=NULL;
			back=NULL;
			start=NULL;
			resolutionOne=NULL;
			resolutionTwo=NULL;
			resolutionThree=NULL;
		}
	}
}